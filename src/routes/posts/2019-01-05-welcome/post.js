export default {
  getImg: () => import("./post.jpg"),
  title: `Bienvenido a nuestro blog, aquí te enterarás de todas las noticias!`,
  tags: ["Locatefit", "News", "Work", "Freelancers"],
  spoiler:
    "Te damos la bienvenida a nuestra plataforma de profesionales freelancers a domicilio.",
  getContent: () => import("./document.mdx")
};
