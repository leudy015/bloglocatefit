export default {
  getImg: () => import("./work.jpg"),
  title: `Hola y bienvenido a Locatefit los mejores profeisonales.`,
  tags: ["Start", "Lanzamiento", "Locatefit", "Profesionales"],
  spoiler:
    "Bienvenido a Locatefit por fin ya hemos hecho el lanzamiento de nuestra plataforma para que disfrutes de las ventajas de contratar profesionales de forma segura.",
  getContent: () => import("./document.mdx")
};
