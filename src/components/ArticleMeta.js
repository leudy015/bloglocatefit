import { join } from "path";
import React from "react";
import { Link } from "react-navi";
import { formatDate } from "../utils/formats";
import styles from "./ArticleMeta.module.css";
import { FaClock } from "react-icons/fa";

function ArticleMeta({ blogRoot, meta, readingTime }) {
  let readingTimeElement;
  if (readingTime) {
    let minutes = Math.max(Math.round(readingTime.minutes), 1);
    let cups = Math.round(minutes / 5);
    readingTimeElement = (
      <>
        &nbsp;&bull;&nbsp;
        <div className={styles.readingTime}>
          {new Array(cups || 1).fill("☕️").join("")} {minutes} Minutos de
          lectura.
        </div>
      </>
    );
  }

  return (
    <div className={styles.ArticleMeta}>
      <span>
        <FaClock />
        &nbsp;
        <time dateTime={meta.date.toUTCString()}>{formatDate(meta.date)}</time>
        <span>&nbsp;&bull;&nbsp;</span>
      </span>
      {meta.tags && meta.tags.length && (
        <ul className={styles.tags}>
          {meta.tags.map(tag => (
            <li key={tag}>
              <Link href={join(blogRoot, "tags", tag)}>{tag}</Link>
            </li>
          ))}
        </ul>
      )}
      {readingTimeElement || null}
    </div>
  );
}

export default ArticleMeta;
