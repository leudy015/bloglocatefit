import React from "react";
import ArticleSummary from "./ArticleSummary";
import Pagination from "./Pagination";
import styles from "./BlogIndexPage.module.css";
import imagen1 from "./../assets/img/imagen1.jpg";
import imagen2 from "./../assets/img/imagen2.jpg";
import imagen3 from "./../assets/img/imagen3.jpg";
import { Link } from "react-navi";

function BlogIndexPage({ blogRoot, pageCount, pageNumber, postRoutes }) {
  return (
    <div className={styles.indexContainer}>
      <section className={styles.indexHeader}>
        <div className={styles.blogTitle}>
          <h1>
            Bienvenidos a nuestro blog entérate de todas las noticias de nuestra
            comunidad
          </h1>
          <p>
            Somos el equipo de prensa, contenido, investigación y diseño de Locatefit.
            Ayudamos a los profesionales a ser contratados y a los clientes a cubrir necesidades. Ven a buscar
            las respuestas con nosotros.
          </p>
          <Link href="https://locatefit.es/become-profesional">Convertirme en profesional</Link>
        </div>
    
        <div className={styles.wave}></div>
        <div className={styles.wave2}></div>
        <div className={styles.wave3}></div>
        <div className={styles.wave4}></div>
      </section>

      <div className={styles.Features}>
        <div className={styles.FeatureContainer}>
          <img src={imagen1} alt="image1" />

          <div className={styles.FeatureText}>
            <h3>Contratar profesionales es así de fácil</h3>
            <p>
              En el equipo de Locatefit, creamos una herramienta que tienen un gran
              impacto con los profesionales. Nos reunimos con profesionales,
              escuchamos sus historias y trabajamos juntos en sistemas complejos
              para construir una herramienta que permita que nuestra red de
              frelancers funcione.
            </p>
          </div>
        </div>

        <div className={styles.FeatureContainer}>
          <img src={imagen3} alt="image1" />

          <div className={styles.FeatureText}>
            <h3>Más de 5 millones de clientes sastisfechos</h3>
            <p>
              Con nuestra herramienta es la forma más fácil de contratar un profesional,
              en ella puedes ver las opiniones de otros clientes, así como todos los detalles
              de sus servicios, aquí pagarás lo justo por el trabajo que quieras realizar.

            </p>
          </div>
        </div>

        <div className={styles.FeatureContainer}>
          <img src={imagen2} alt="image1" />

          <div className={styles.FeatureText}>
            <h3>Un equipo dedicado a diseñar la mejor herramienta</h3>
            <p>
              Nuestra misión es aumentar los ingresos de los profesionales. Por ello
              nos esforzamos en construir la mejor herramienta y más simple de usar
              para que cualquier persona pueda contratar un profesional. En Locatefit
              no lo llamamos trabajo, sino hobbie.
            </p>
          </div>
        </div>
      </div>

      <div className={styles.blogPosts}>
        <div className={styles.blogPostsHeader}>
          <h1>Lee nuestro blog</h1>
          <p>
            Cosas en las que estamos trabajando, eventos a los que vamos,
            productos que usamos y personas que conocemos.
          </p>
        </div>

        <ul className={styles.articlesList}>
          {postRoutes.map(route => (
            <li key={route.url.href}>
              <ArticleSummary blogRoot={blogRoot} route={route} />
            </li>
          ))}
        </ul>
        {pageCount > 1 && (
          <Pagination
            blogRoot={blogRoot}
            pageCount={pageCount}
            pageNumber={pageNumber}
          />
        )}
      </div>
 
    </div>
  );
}

export default BlogIndexPage;
