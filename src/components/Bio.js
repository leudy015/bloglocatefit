import React from "react";
import styles from "./Bio.module.css";
import { getGravatarURL } from "../utils/getGravatarURL";
import { FaTwitter, FaUser } from "react-icons/fa";

function Bio(props) {
  let photoURL = getGravatarURL({
    email: "leudy1521@gmail.com",
    size: 40
  });

  return (
    <div
      className={`
      ${styles.Bio}
      ${props.className || ""}
    `}
    >
      <img src={photoURL} alt="Me" />
      <div>
        <a href="https://twitter.com/leudymartes/">
          <FaTwitter /> Leudy Martes
        </a>
        <p className={styles.desc}>
          <FaUser /> CEO en Locatefit
        </p>
      </div>
    </div>
  );
}

export default Bio;
